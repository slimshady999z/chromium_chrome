// Copyright 2021 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// A module for a prototype of the Reader Mode feature.
module reader_mode.mojom;

// Used by the WebUI page to bootstrap bidirectional communication.
interface PageHandlerFactory {
  // The WebUI calls this method when the page is first initialized.
  CreatePageHandler(pending_receiver<PageHandler> handler);
};

// Browser-side handler for requests from WebUI page.
interface PageHandler {
  // Shows Reader Mode. Result is a list of paragraphs from a web page, where
  // each paragraph is considered essential (is part of the main content of the
  // web page).
  ShowReaderMode() => (array<string> result);
};
