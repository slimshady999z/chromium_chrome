// Copyright 2020 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package org.chromium.chrome.browser.base;

// TODO(b/b/183478921): Remove this once all references are removed in internal repos.
public class SplitCompatUtils extends org.chromium.base.BundleUtils {}
